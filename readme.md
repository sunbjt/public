# 讲师介绍


* 刘思喆，51Talk（COE）首席大数据专家，负责数据平台建设、流程算法优化以及数据分析相关团队管理及技术指导工作。
* 毕业于中国人民大学统计学院，13年数据领域工作经验
* 中国R语言会议，COS数据科学沙龙联合发起人，多年电信、互联网数据挖掘咨询顾问经历
* 曾任京东推荐平台部高级经理 ，主要负责全渠道（PC、App、微信、手Q）详情页、购物车页、订单完成页等近一百个位置的商品推荐算法和策略的改进，以及用户行为分析系统、商品画像体系、商品关系图谱等内容。
* 带领团队将推荐订单占全京东订单的占比从5%提升到10%。在《京东技术解密》一书中，是京东研发体系的15位技术牛人之一，数据达人称号。
* 同时他还是中国人民大学大数据分析实验班、首经贸信息学院校外硕士生导师。
* SDCC推荐专场出品人，Qcon2013（上海）优秀讲师，51CTO、CSDN 大数据技术特邀讲师，DATA World Forum特邀讲师，人大经济论坛特邀讲师
* 15年的使用经验，是《153分钟学会R》的作者以及《R核心技术手册》的译者。
* 个人博客：http://bjt.name


# 课程计划

## Day 1:

1. Introduction to R (0.5H)
2. R Fundamentals (4.0H)

	- 数据结构和基本操作
	- 数据的运算
	- 汇总和高级处理方式
	- data.table包
	- 数据流的管道操作

3. 可重复性的自动化报告及相关 (1.5H)

## Day 2：

1. 数据库和R的协同工作方式 (1.0H)
2. 数据可视化：统计绘图和ggplot2 (2.0H)
3. 传统统计模型 (3.0H)

	- 方差分析/ANONA
	- 对应分析/Correspondent analysis
	- 主成份和因子分析/Principal Components or Factor Analysis
	- 判别分析/Linear Discrimination Analysis
	- 多维尺度分析/Multidimensional Scaling
	- 生存分析/Survival
	- 回归（最小二乘、广义线性、非线性、分位数、逻辑回归）/Regression(Least Squares, Generalized Linear Models, Nonlinear Least Squares, Quantile Regression, Logistic Regression)
	- 时间序列预测/Time Series Analysis
	- 蒙特卡洛模拟/Monte Carlo method	

## Day 3:

4. 数据挖掘方法/Data Mining Techniques（8H）
	- 数据库和R/Data Bases and R
	- 聚类分析（K均值、层次聚类、模糊聚类等）/Clustering(K means, Fuzzy Clustering)
	- 推荐系统(关联规则与协同过滤)/Recommendation System(Association Rules, Collaborative Filtering)
	- 传统分类模型（knn、贝叶斯、决策树、支持向量机等）/Classification(Bayes, Decision Trees, SVMs)
	- 高阶分类器（Lasso及衍生、随机森林、提升树）/Lasso、Random Forest, Gradient Boosting Machine
	- 线性分类器的特征工程的应用/Feature engineering for Linear Model
	- 神经网络和深度学习/Neural Networks and Deep Learning
	- 数据挖掘（分类模型）项目的评估/Evaluation Of Classifiers
	- 支持高并发的模型部署/Model Deployment
	
5. 文本挖掘/Text Mining（1H）
	- 中文分词简介/Introduction
	- 文本挖掘概念及要点/Concept
	- 常用的爬虫技术/web scraping
	- 词向量表征以及应用/word embedding
	- 主题模型/Topic Model
	
6. 案例/Cases（穿插，2H）
	- 用户分群及预测 + 趋势判断（预警）模型/Customer Churn and Trend Recognition
	- 新闻自动分类引擎的构建/News Automatic Classification
	- 垃圾短信识别/Spam detection
	- 小型歌曲推荐系统（或迅速完成百万级规模用户的个性化邮件推送）/Songs Recommendation
	- 电商平台下-异常商品识别/Abnormal goods detection
	- Target公司案例/How Target Knew a High School Girl Was Pregnant Before Her Parents Did
	- 明朝那些事儿的那些事儿/The People's relationship in Ming Dynasty
	- 价格弹性/Price elasticity of demand
	- 其他

## 案例

穿插在第2、3日

- 用户分群及预测 + 趋势判断（预警）模型/Customer Churn and Trend Recognition
- 新闻自动分类引擎的构建/News Automatic Classification
- 垃圾短信识别/Spam detection
- 小型歌曲推荐系统（或迅速完成百万级规模用户的个性化邮件推送）/Songs Recommendation
- 电商平台下-异常商品识别/Abnormal goods detection
- 明朝那些事儿的那些事儿/The People's relationship in Ming Dynasty
- 价格弹性/Price elasticity of demand


# 工具介绍

1997年，R语言在新西兰的奥克兰大学诞生，专注于统计计算和统计图形两大数据分析领域。经过十多年的发展，R语言的各项功能日趋成熟，广泛的活跃于数据分析的各个行业分支。保守估计，全球范围约有超过两百万的分析师、统计学家、数据科学家在应用R 语言，并通过这个平台在企业中获得利润或节约成本。
这些企业其中包括Microsoft、Google和Facebook等，甚至一些大型的制药企业，例如Pfizer、Johnson\&Johnson等。

R是一个世界范围统计工作者共同协作的产物，至2017年1月底共计近9955个包可在互联网上自由下载，这些都是各行业数据分析同行的工作结晶。来自于2011-16年连续五年的KDnugget的数据分析、数据挖掘领域的公开调研显示，R 语言是最流行的数据分析环境（语言），灵活的数据分析方式、开放的接口、成熟的协作社区是其成为数据分析领域最佳选择的三个重要的原因。

现代数据分析（Exploratory Data Analysis）同传统的数据分析理念有着巨大差异，很难想象：在数据如此丰富多样化的时代，一个不会编程的数据分析师能够轻松地胜任本职工作。而R语言以近乎完美的方式平衡了当前数据分析同程序开发的矛盾，兼顾了数据分析自身灵活性和运行效率，以及正式的和非正式的编程环境。



# 注意事项

1. 课件会提前发给学员。课堂练习代码可以在 https://bitbucket.org/sunbjt/ 的`code`文件夹内下载，该文件夹可在线访问（培训期间可见）
2. 由于课程需要学员实际操作练习，故请携带电脑，并在培训前下载安装R(https://mirrors.tuna.tsinghua.edu.cn/CRAN/bin/windows/base/)，并安装相应扩展包(打开R，并粘贴以下命令，完成自动更新。正常3分钟更新完毕，如果更新较慢，可以Esc重新选择就近的mirror下载)

```r
z <- readLines(
textConnection(
"apcluster
arules
arulesSequences
recommenderlab
e1071
glmnet
kernlab
lars
neuralnet
party
plyr
sqldf
tree
rpart
randomForest
bnlearn
RWeka
slam
coin
boot
knitr
corrplot
ggplot2
maptools
formatR
stringr
devtools
maps
httr
RCurl
XML
caret
ROCR
maxent
topicmodels
ElemStatLearn
rattle"))
install.packages(z)
```

3. 备选：安装rJava需要下载安装Oracle JDK环境，在安装完毕后（以64位为例），需添加：

- 增加 JAVA_HOME = C:\Program Files\Java\jdk1.8.0_25
- 增加 R_HOME = D:\R\R-3.3.2\
- 并在环境变量里添加 `C:\Program Files\Java\jdk1.8.0_25\bin;D:\R\R-3.3.2\bin\x64;D:\R\R-3.3.2\library\rJava\jri\x64;C:\Program Files\Java\jdk1.8.0_25\jre\bin;C:\Program Files\Java\jdk1.8.0_25\jre\bin\server;C:\Program Files\Java\jdk1.8.0_25\lib`
